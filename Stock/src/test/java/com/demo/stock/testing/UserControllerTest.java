package com.demo.stock.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.demo.stock.controller.UserController;
import com.demo.stock.dto.PurchaseDto;
import com.demo.stock.dto.ResponseMessage;
import com.demo.stock.entity.User;
import com.demo.stock.exception.UserException;
import com.demo.stock.repository.UserRepository;
import com.demo.stock.service.UserService;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class UserControllerTest {

	
	public ResponseEntity<String> loginCustomer(@RequestBody User user) throws UserException {
		return new ResponseEntity<String>(userService.login(user), HttpStatus.OK);
	}
	
	
	@Mock
	UserService userService;
	
	@InjectMocks
	UserController userController;
	
	
	User user= new User();
	
	PurchaseDto purchaseDto=new PurchaseDto();
	
	ResponseMessage response= new ResponseMessage();
	
	
	@Before(value = "")
	public void setup() {
		user.setUserId(1);
		user.setUserName("latha");
		user.setPassword("1234");
		
	}
	
	
	@Test
	public void loginTest() {
		when(userService.findByUserNameAndPassword().thenReturn(response.getStatusCode(),response.getMessage()));
		assertEquals(1, 200, "login Success");
		Mockito.verify(userService,times(1),loginCustomer(user));
	}
	
	
	

	
	
	
}
