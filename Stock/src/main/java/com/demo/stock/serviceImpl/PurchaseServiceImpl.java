package com.demo.stock.serviceImpl;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.stock.dto.PurchaseDto;
import com.demo.stock.dto.ResponseMessage;
import com.demo.stock.entity.Purchase;
import com.demo.stock.repository.PurchaseRepository;
import com.demo.stock.service.PurchaseService;
import com.demo.stock.utility.ErrorConstant;

@Service
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	PurchaseRepository purchaseRepository;

	private Purchase purchase;
	private PurchaseDto purchaseDto;
	private ResponseMessage responseMessage;

	public PurchaseServiceImpl() {
		purchase = new Purchase();
		purchaseDto = new PurchaseDto();
		responseMessage = new ResponseMessage();

	}

	@Override
	public PurchaseDto purchaseStock(PurchaseDto request) {

		purchase.setUserId(request.getUserId());
		purchase.setPurchaseId(request.getPurchaseId());
		purchase.setStockInformationId(request.getStockId());
		purchase.setStockName(request.getStockName());
		purchase.setQuantity(request.getQuantity());
		purchase.setPrice(request.getPrice());
		//setTotalPrice(calculateTotalPrice(request.getPrice(), request.getQuantity()));
		purchase.setTotalPrice(request.getTotalPrice());

		Purchase saved = purchaseRepository.save(purchase);
		if (Objects.nonNull(saved)) {

			purchaseDto.setUserId(saved.getUserId());
			purchaseDto.setStockId(saved.getStockInformationId());
			purchaseDto.setQuantity(saved.getQuantity());
			purchaseDto.setTotalPrice(saved.getTotalPrice());
			purchaseDto.setPrice(saved.getPrice());
			purchaseDto.setPurchaseId(saved.getPurchaseId());
			purchaseDto.setStockName(saved.getStockName());
			purchaseDto.setMessage(saved.getMessage());
			responseMessage.setStatusCode(ErrorConstant.PURCHASE_CODE_CREATED);
			responseMessage
					.setMessage(String.format(ErrorConstant.PURCHASE_PLACED_MESSAGE_SUCCESS, saved.getStockName()));
			purchaseDto.setMessage(responseMessage);
		}
		return purchaseDto;
	}

	/*
	 * private Double calculateTotalPrice(Double price, Integer quantity) {
	 * 
	 * price = price.multiply(Double.valueOf(quantity)); total = price; return
	 * totalPrice; }
	 */
	private String generatePurchaseId() {
		AtomicInteger integer = new AtomicInteger(100);
		int incrementAndGet = integer.incrementAndGet();
		return String.valueOf(incrementAndGet);
	}
}
