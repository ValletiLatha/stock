package com.demo.stock.dto;

public class ResponseMessage {
	@Override
	public String toString() {
		return "ResponseMessage [statusCode=" + statusCode + ", message=" + message + "]";
	}

	private String statusCode;
	private String message;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
