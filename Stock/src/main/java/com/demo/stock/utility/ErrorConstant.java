package com.demo.stock.utility;

public class ErrorConstant {
public static final String PURCHASE_CODE_CREATED = "201";
	
	public static final String PURCHASE_PLACED_MESSAGE_SUCCESS = "Your purchase is done successfuly for %s.";

	public static final String STOCK_RECORD_NOT_FOUND = "StockInformation not found";
	public static final String USER_LOGIN_ERROR = "login error";
	public static final String USER_LOGIN_SUCCESS = "login success";
	public static final String STOCK_NOT_FOUND = "Stock not found";
}
