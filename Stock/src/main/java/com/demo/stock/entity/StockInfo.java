package com.demo.stock.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StockInfo {
 
	
	@Id
	private int stockInformationId;
	private int stockId;
	private String stockName;
	private int quantity;
	private double price;
	private boolean isPresent;
	public int getStockInformationId() {
		return stockInformationId;
	}
	public void setStockInformationId(int stockInformationId) {
		this.stockInformationId = stockInformationId;
	}
	public int getStockId() {
		return stockId;
	}
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean isPresent() {
		return isPresent;
	}
	public void setPresent(boolean isPresent) {
		this.isPresent = isPresent;
	}
	
}
