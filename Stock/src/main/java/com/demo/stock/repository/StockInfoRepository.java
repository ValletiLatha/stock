package com.demo.stock.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.stock.entity.StockInfo;

@Repository
public interface StockInfoRepository extends JpaRepository<StockInfo,Integer>{

	Optional<StockInfo> findByStockInformationId(int stockInformationId);

}
