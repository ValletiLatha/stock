package com.demo.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.stock.entity.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock,Integer> {

}
