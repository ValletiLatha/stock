package com.demo.stock.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.stock.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User,Integer>{

	List<User> findByUserNameAndPassword(String userName, String password);

	List<User> findByUserName(String userName);

}
