package com.demo.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.stock.entity.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase,Integer> {

}
