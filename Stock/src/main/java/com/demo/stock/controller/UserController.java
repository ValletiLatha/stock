package com.demo.stock.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.stock.dto.ResponseMessage;
import com.demo.stock.entity.User;
import com.demo.stock.exception.UserException;
import com.demo.stock.service.UserService;


@RestController
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/login")
	public ResponseEntity<ResponseMessage> loginCustomer(@RequestBody User user) throws UserException {
		return new ResponseEntity<ResponseMessage>(HttpStatus.OK);
	}
}
