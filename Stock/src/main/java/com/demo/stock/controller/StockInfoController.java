package com.demo.stock.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.stock.entity.StockInfo;
import com.demo.stock.exception.StockInfoException;
import com.demo.stock.service.StockInfoService;



@RestController
@RequestMapping("/stock")
public class StockInfoController {

	
	@Autowired
	StockInfoService stockInfoService;
	

	@GetMapping("/stocksDetails")
	private ResponseEntity<List<StockInfo>> getAllStocks() throws StockInfoException  {
		
		return new ResponseEntity<List<StockInfo>>(stockInfoService.getAllStocks(),HttpStatus.FOUND);
	}
	
	@GetMapping("/stocks/{stockInformationId}")
	private ResponseEntity<StockInfo> getPassengerByEmail(@PathVariable("stockInformationId") int stockInformationId) throws StockInfoException  {
			
			return new ResponseEntity<StockInfo>(stockInfoService.getByStockInformationId(stockInformationId),HttpStatus.FOUND);
		}
	
	
}


