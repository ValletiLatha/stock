package com.demo.stock.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.stock.dto.PurchaseDto;
import com.demo.stock.dto.PurchaseOrderRequest;
import com.demo.stock.service.PurchaseService;


@RestController
public class PurchaseController {

	
	@Autowired
	PurchaseService purchaseService;
	
	@PostMapping(path = "/users/purchase")
	public ResponseEntity<PurchaseDto> purchaseStock(@RequestBody PurchaseDto request) {
		Objects.requireNonNull(request);
		PurchaseDto purchaseStock = purchaseService.purchaseStock(request);
		return new ResponseEntity<PurchaseDto>(purchaseStock, HttpStatus.OK);
	}
	
	@GetMapping(path = "/users/purchases/stocks")
	public ResponseEntity<PurchaseOrderRequest> getPurchaseSample() {
		PurchaseOrderRequest request = new PurchaseOrderRequest();
		request.setUserId(123);
		request.setStockId(1);
		request.setStockName("ICICI");
		request.setQuantity(1);
		request.setPrice(200);
		request.setTotalAmount(2000);
		return new ResponseEntity<>(request, HttpStatus.OK);
	}
}
