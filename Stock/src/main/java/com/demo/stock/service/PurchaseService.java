package com.demo.stock.service;

import com.demo.stock.dto.PurchaseDto;


public interface PurchaseService {

	public PurchaseDto purchaseStock(PurchaseDto purchaseDto);
		
	}


