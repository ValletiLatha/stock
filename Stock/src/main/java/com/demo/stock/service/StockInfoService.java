package com.demo.stock.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.stock.entity.StockInfo;
import com.demo.stock.exception.StockInfoException;
import com.demo.stock.repository.StockInfoRepository;
import com.demo.stock.utility.ErrorConstant;

@Service
public class StockInfoService {

	@Autowired 
	StockInfoRepository stockInfoRepository;
	
	public List<StockInfo> getAllStocks() throws StockInfoException {
		
			  List<StockInfo> stockList=stockInfoRepository.findAll();
			  if(stockList.isEmpty())
			  {
				  throw new StockInfoException(ErrorConstant.STOCK_RECORD_NOT_FOUND);
			  }
			  else {
		   return  stockList;
		   }
		   }

	public StockInfo getByStockInformationId(int stockInformationId) throws StockInfoException {
		// TODO Auto-generated method stub
			Optional<StockInfo> stockInfo=stockInfoRepository.findByStockInformationId(stockInformationId);
			if(!stockInfo.isPresent())
			{
				throw new StockInfoException(ErrorConstant.STOCK_NOT_FOUND);
			}
			else
			{
			 return stockInfo.get();
		  }
		  
		}
	}
		
	


