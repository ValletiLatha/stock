package com.demo.stock.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.stock.entity.User;
import com.demo.stock.exception.UserException;
import com.demo.stock.repository.UserRepository;
import com.demo.stock.utility.ErrorConstant;


@Service
public class UserService {

	@Autowired 
	UserRepository userRepository;
	@Autowired
	EntityManager entityManager;
	
	public String login(User user) throws UserException {
		List<User> userList = userRepository.findByUserNameAndPassword(user.getUserName(), user.getPassword());

		if (userList.isEmpty()) {
		throw new UserException(ErrorConstant.USER_LOGIN_ERROR);
	

		} else {
     // Optional<User> user =userRepository.findByUserName(user.getUserName());
       Session session=entityManager.unwrap(Session.class);
		
		  Transaction transaction=session.beginTransaction();
	//	session.save(user.get()); 
		transaction.commit();
		return ErrorConstant.USER_LOGIN_SUCCESS;
		}
	}
}
	
