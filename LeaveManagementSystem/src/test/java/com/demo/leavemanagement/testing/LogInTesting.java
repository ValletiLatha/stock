package com.demo.leavemanagement.testing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.demo.leavemanagement.controller.UserController;
import com.demo.leavemanagement.dto.LoginDto;
import com.demo.leavemanagement.dto.Response;
import com.demo.leavemanagement.exception.UserException;
import com.demo.leavemanagement.service.UserService;

@RunWith(org.mockito.junit.MockitoJUnitRunner.Silent.class)
public class LogInTesting {

	public ResponseEntity<String> loginUser(@RequestBody LoginDto loginDto) throws UserException {
		return new ResponseEntity<String>(userService.login(loginDto), HttpStatus.OK);
	}

	@Mock
	UserService userService;

	@InjectMocks
	UserController userController;

	LoginDto loginDto = new LoginDto();

	String response = new String("login succcessfully");

	@Before
	public void setup() {
		loginDto.setEmail("lucky@gmail.com");
		loginDto.setPassword("1234");
	}

	@Test 
	public void loginTests() {
		when(userService.login(loginDto)).thenReturn(response);
		ResponseEntity<String> userController.loginUser(loginDto);        
		Mockito.verify(userService, times(1)).login(loginDto);

	}

}
