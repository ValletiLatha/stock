package com.demo.leavemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.leavemanagement.entity.ApplyLeave;
import com.demo.leavemanagement.entity.LeaveType;

@Repository
public interface LeaveRepository extends JpaRepository<ApplyLeave,Integer>{

	

	ApplyLeave save(ApplyLeave applyLeave);

	
	
}
