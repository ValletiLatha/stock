package com.demo.leavemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.leavemanagement.entity.Leave;
import com.demo.leavemanagement.entity.LeaveType;

@Repository
public interface ApplyLeaveTypeRepository extends JpaRepository<LeaveType,Integer>{

	List<LeaveType> fetchUserData(int userId);

	List<LeaveType> getleaveDetails(int userId);

	






}
