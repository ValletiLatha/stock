package com.demo.leavemanagement.dto;

import java.sql.Date;

public class ApplyLeaveDto {
	private int userId;
	private Date startDate;
	private Date endDate;
	private int remainingLeaves;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getRemainingLeaves() {
		return remainingLeaves;
	}
	public void setRemainingLeaves(int remainingLeaves) {
		this.remainingLeaves = remainingLeaves;
	}
	public void setMessage(Response response) {
		// TODO Auto-generated method stub
		
	}
	
}
