package com.demo.leavemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.demo.leavemanagement.dto.LoginDto;
import com.demo.leavemanagement.entity.User;
import com.demo.leavemanagement.exception.UserException;
import com.demo.leavemanagement.repository.UserRepository;
import com.demo.leavemanagement.utility.ErrorConstant;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public String login(LoginDto loginDto) throws UserException {
		List<User> userList = userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword());

		if (userList.isEmpty()) {
			throw new UserException(ErrorConstant.USER_LOGIN_ERROR);

		} else {
			return ErrorConstant.USER_LOGIN_SUCCESS;

		}

	}
}
