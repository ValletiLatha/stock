package com.demo.leavemanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.leavemanagement.dto.ApplyLeaveDto;
import com.demo.leavemanagement.dto.Response;
import com.demo.leavemanagement.entity.ApplyLeave;
import com.demo.leavemanagement.entity.LeaveType;
import com.demo.leavemanagement.repository.ApplyLeaveTypeRepository;
import com.demo.leavemanagement.repository.LeaveRepository;
import com.demo.leavemanagement.utility.ErrorConstant;


@Service
public class LeaveService {
	@Autowired
	LeaveRepository leaveRepository;
	
	@Autowired
	ApplyLeaveTypeRepository ltRepository;
	
	private ApplyLeave applyLeave;
	private ApplyLeaveDto applyLeaveDto;
	private Response response;
	
	public LeaveService() {
		applyLeave=new ApplyLeave();
		applyLeaveDto=new ApplyLeaveDto();
	     response=new Response();
	}

	public List<LeaveType> fetchUserData(int userId)  {
		List<LeaveType> lf = new ArrayList<LeaveType>();
		lf = ltRepository.getleaveDetails(userId);
		if(!lf.isEmpty()) {
			return lf;
		}else {
			throw new ServiceException(ErrorConstant.NO_DATA_FOUND_WITH_USERID);
		}
	}


	public ApplyLeaveDto applyleaves(ApplyLeaveDto leaveDto) {
	
			applyLeave.setUserId(leaveDto.getUserId());
			applyLeave.setStartDate(leaveDto.getStartDate());
			applyLeave.setEndDate(leaveDto.getEndDate());
			applyLeave.setRemainingLeaves(leaveDto.getRemainingLeaves());
			
			ApplyLeave saved = leaveRepository.save(applyLeave);
			if(Objects.nonNull(saved)) {
				applyLeaveDto.setUserId(saved.getUserId());
				applyLeaveDto.setStartDate(saved.getStartDate());
				applyLeaveDto.setEndDate(saved.getEndDate());
				applyLeaveDto.setRemainingLeaves(saved.getRemainingLeaves());
				response.setStatus(200);
				response.setMessage( response.format(ErrorConstant.LEAVE_MESSAGE_SUCCESS, saved.getUserId()));
				applyLeaveDto.setMessage(response);
			}
			return applyLeaveDto;
		}
	}


