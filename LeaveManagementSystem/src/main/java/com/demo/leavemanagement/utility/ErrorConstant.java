package com.demo.leavemanagement.utility;

public class ErrorConstant {
public static final String USER_LOGIN_ERROR = "invalid username or password";
public static final String USER_LOGIN_SUCCESS = "logged in successfully";
public static final String NO_DATA_FOUND_WITH_USERID = "no data found";
public static final String SUCCESS = "success";
public static final String LEAVE_MESSAGE_SUCCESS = "no data found";
}




