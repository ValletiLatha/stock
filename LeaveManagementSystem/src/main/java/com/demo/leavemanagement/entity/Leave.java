package com.demo.leavemanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Leave {
    @Id
	private int leaveId;
	private String leaveType;
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	
}
