package com.demo.leavemanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LeaveType {

	@Id
	private int leaveId;
	private String restricted;
	private String annual;
	private String myLeave;
	private int userId;

	public int getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	public String getRestricted() {
		return restricted;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setRestricted(String restricted) {
		this.restricted = restricted;
	}

	public String getAnnual() {
		return annual;
	}

	public void setAnnual(String annual) {
		this.annual = annual;
	}

	public String getMyLeave() {
		return myLeave;
	}

	public void setMyLeave(String myLeave) {
		this.myLeave = myLeave;
	}

	
}
