package com.demo.leavemanagement.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.leavemanagement.dto.ApplyLeaveDto;

import com.demo.leavemanagement.entity.LeaveType;
import com.demo.leavemanagement.repository.ApplyLeaveTypeRepository;
import com.demo.leavemanagement.service.LeaveService;


@RestController
public class LeaveController {

	@Autowired 
	LeaveService leaveService;
	@Autowired
	ApplyLeaveTypeRepository ltRepository;
	
	@GetMapping("/leaves/{userId}")
	public  ResponseEntity<List<LeaveType>> getleaveDetails(@PathVariable("userId") int userId){
		return new ResponseEntity<List<LeaveType>>(leaveService.fetchUserData(userId),HttpStatus.OK);
	}
	
	
	@PostMapping(path = "/users/applyleaves")
	public ResponseEntity<String> applyleave(@RequestBody ApplyLeaveDto leaveDto) {
		Objects.requireNonNull(leaveDto);
		ApplyLeaveDto applyleaves = leaveService.applyleaves(leaveDto);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

}
