package com.demo.leavemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.leavemanagement.dto.LoginDto;
import com.demo.leavemanagement.exception.UserException;
import com.demo.leavemanagement.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/login")
	public ResponseEntity<String> loginUser(@RequestBody LoginDto loginDto) throws UserException {
		return new ResponseEntity<String>(userService.login(loginDto), HttpStatus.OK);
	}

}
